import { Component, OnInit } from '@angular/core';
import { GenericService } from '../generic.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})

export class HomeComponent implements OnInit {
  data: any = [] ;
  data1: any;
  headElements = [ 'ID', 'Descripcion', 'Nif', 'Email', 'Telefono', 'Login' ];

  constructor(private HomeService: GenericService) { }

  ngOnInit() {
    this.HomeService.getMesas('local').subscribe(
      data => {
        this.data = data;
      }
    );
  }

delete(id) {
  this.HomeService.deleteLocal('local', id).subscribe(
    data1 => {
      this.data1 = data1;
      this.ngOnInit();
    }  );

}
}
