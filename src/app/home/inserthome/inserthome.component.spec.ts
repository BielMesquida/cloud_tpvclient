import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InserthomeComponent } from './Inserthome.Component';

describe('InserthomeComponent', () => {
  let component: InserthomeComponent;
  let fixture: ComponentFixture<InserthomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InserthomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InserthomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
