import { Component, OnInit } from '@angular/core';
import { GenericService } from 'src/app/generic.service';
import { Validators, FormGroup, FormControl, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-inserthome',
  templateUrl: './inserthome.component.html',
  styleUrls: ['./inserthome.component.css']
})
export class InserthomeComponent implements OnInit {

  formularioLocal: FormGroup

  data: any;


  constructor(private GenericService: GenericService,
               private FormBuilder: FormBuilder) {

                this.formularioLocal = this.FormBuilder.group({

                  descripcion: ['', Validators.required],
                  nif:  ['', Validators.required],
                  email:  ['', Validators.required],
                  telefono: ['', Validators.required],
                  login:  ['', Validators.required],
                  password: ['', Validators.required],
                });
            }
  ngOnInit() { }

onSubmit() {

this.GenericService.insertarLocal('local', this.formularioLocal.value).subscribe(
    data => {
      this.data = data;
    }
);
    }

}




