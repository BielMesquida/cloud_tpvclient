import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { GenericService } from 'src/app/generic.service';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-updatehome',
  templateUrl: './updatehome.component.html',
  styleUrls: ['./updatehome.component.css']
})
export class UpdatehomeComponent implements OnInit {

  formularioLocal: FormGroup;
  data: any;
  descripcion: any;


  constructor(private GenericService: GenericService,
              private FormBuilder: FormBuilder,
              private rutaActiva: ActivatedRoute) {

                this.formularioLocal = this.FormBuilder.group({
                  id: [this.rutaActiva.snapshot.params.id, Validators.required],
                  descripcion: ['', Validators.required],
                  nif:  ['', Validators.required],
                  email:  ['', Validators.required],
                  telefono: ['', Validators.required],
                  login:  ['', Validators.required],
                  password: ['', Validators.required],
                });
            }

            ngOnInit() {
              this.GenericService.getLocalById('local', this.rutaActiva.snapshot.params.id
                ).subscribe(
                data => {
                  this.data = data;
                  this.formularioLocal = this.FormBuilder.group({
                    id: [this.rutaActiva.snapshot.params.id, Validators.required],
                    descripcion: [data.descripcion, Validators.required],
                    nif:  [data.nif, Validators.required],
                    email:  [data.email, Validators.required],
                    telefono: [data.telefono, Validators.required],
                    login:  [data.login, Validators.required],
                    password: ['', Validators.required],
                  });
              }
              );

            }

onSubmit() {

this.GenericService.updateLocal('local', this.formularioLocal.value).subscribe(
    data => {
      this.data = data;
    }
);
    }

}


