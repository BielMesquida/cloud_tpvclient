import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { pruebaResolver } from './resolvers/prueba.resolver';

@Injectable({
  providedIn: 'root'
})
export class CanactivateGuard implements CanActivate {
 /* canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return true;
  }*/
  constructor( private _ac: ActivatedRoute, private router: Router) {}
  canActivate() {

    if ( this._ac.snapshot.data.pruebaResolver === null) {
        return false;
    }
    return true;
  }
}
