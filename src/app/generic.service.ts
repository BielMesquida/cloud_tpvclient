import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class GenericService {
  baseUrl = 'http://localhost:8082/';

  constructor(private http: HttpClient ) { }
  getMesas(tabla): Observable<any> {
    return this.http.get(this.baseUrl + tabla + '/getall');
  }
  get(tabla, localid): Observable<any> {
    return this.http.get(this.baseUrl + tabla + '/filter/' + localid );
  }
  getProductos(tabla, localid, page, rpp): Observable<any>{
    return this.http.get(this.baseUrl + tabla + '/filter/' + page + '/' + rpp + '/' + localid );
  }
  deleteLocal(tabla, id): Observable<any> {
    return this.http.delete('http://localhost:8082/' + tabla + '/' + id).pipe(retry(1),catchError(this.handleError));
  }
  insertarLocal(tabla, localData: any): Observable<any> {
    return this.http.post('http://localhost:8082/' + tabla + '/', localData);
  }
  updateLocal(tabla, localData: any): Observable<any> {
    return this.http.put('http://localhost:8082/' + tabla + '/', localData);
  }
  getLocalById(tabla, id): Observable<any> {
    return this.http.get('http://localhost:8082/' + tabla + '/' + id);
  }
  getCount(tabla): Observable<any>{
    return this.http.get('http://localhost:8082/count/' + tabla) ;
  }
  getLp(tabla, id): Observable<any>{
    return this.http.get('http://localhost:8082/' + tabla + '/lp/' + id);
  }

  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
        // client-side error
        errorMessage = `Error: ${error.error.message}`;
    } else {
        // server-side error
        errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(errorMessage);
}

}
