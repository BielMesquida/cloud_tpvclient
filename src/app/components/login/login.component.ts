import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl, FormBuilder } from '@angular/forms';
import { LoginService } from '../../services/login.service';
import { Router } from '@angular/router';
import { AppComponent } from '../../app.component';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  constructor(private LoginService: LoginService,
              private FormBuilder: FormBuilder,
              private router: Router,
              private _snackBar: MatSnackBar
              )
                {}

  public data: any;
  formularioLogin: FormGroup;
  public dataCheck: any;

ngOnInit() {
  this.formularioLogin = this.FormBuilder.group({
    login : ['', Validators.compose([Validators.required])],
    password : ['', Validators.required]
  });
 }
onSubmit() {
  const formData: any = new FormData();
  formData.append('login', this.formularioLogin.get('login').value);
  formData.append('password', this.formularioLogin.get('password').value);
  this.LoginService.getLogin(formData).subscribe(
      data => {
        this.data = data;
        if (this.data != null) {
          this.router.navigate(['/mesas']);
        } else{
            this.openSnackBar();
        }
      }

      );

    }
    openSnackBar() {
      this._snackBar.open('Usuario o contraseña incorrectos','Aceptar', {
        duration: 3000,
      });
    }


}





