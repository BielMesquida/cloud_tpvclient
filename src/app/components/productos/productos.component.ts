import { Component, OnInit, SimpleChange, SimpleChanges, OnChanges, Input, AfterViewInit, ViewChild } from '@angular/core';
import { GenericService } from 'src/app/generic.service';
import { LoginService } from 'src/app/services/login.service';
import { ActivatedRoute, Router } from '@angular/router';
import { PageEvent, MatPaginator, MatDialog, MatTable } from '@angular/material';
import { PaginacionComponent } from '../paginacion/paginacion.component';
import { DialogBoxComponent } from '../../dialog-box/dialog-box.component';

@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.css']
})
export class ProductosComponent implements OnInit {

  constructor(private GenericService: GenericService, private LoginService: LoginService,
              private _ac: ActivatedRoute, private router: Router, private pageEvent: PageEvent,
              public dialog: MatDialog
    ) {


    }
    @ViewChild(MatTable,{static:true}) table: MatTable<any>;

  //  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  data: any;
  dataCheck: any;
  data1: any;
  data12: any;
  headElements = [ 'Codigo', 'Precio', 'Imagen', 'Descripcion', 'Tipo producto', 'Acciones' ];
  length = 100;
  pageSizeOptions: number [] = [ 10, 5, 25, 100];
  ngOnInit() {
        if ( this._ac.snapshot.data.pruebaResolver === null) {
      this.router.navigate(['/home']);
   }
    this.GenericService.getProductos('producto', this._ac.snapshot.data.pruebaResolver.id,  0, 10 )
    .subscribe(
     data => {
       this.data = data.content;
       this.data12 = data.totalElements;
});
  }

  setPage(e:PageEvent){
    this.pageEvent.pageIndex = e.pageIndex;
    this.pageEvent.pageSize = e.pageSize;
    this.GenericService.getProductos('producto', this._ac.snapshot.data.pruebaResolver.id,
    this.pageEvent.pageIndex, this.pageEvent.pageSize ).subscribe(
      data => {
        this.data = data.content;
        this.data12 = data.totalElements;
});
  }

getFilter() {

}

delete(id) {
  this.GenericService.deleteLocal('ticket', id).subscribe(
    data1 => {
      this.data1 = data1;
      this.ngOnInit();
    }  );

}

openDialog(action,obj) {
  obj.action = action;
  const dialogRef = this.dialog.open(DialogBoxComponent, {
    width: '250px',
    data:obj
  });
  dialogRef.afterClosed().subscribe(result => {
    if(result.event == 'Add'){
      this.addRowData(result.data);
    }else if(result.event == 'Update'){
      this.updateRowData(result.data);
    }else if(result.event == 'Delete'){
      this.deleteRowData(result.data);
    }
  });
}
addRowData(row_obj){
  var d = new Date();
  this.data.push({
    descripcion: row_obj.descripcion,
    tipo_producto: row_obj.tipo_producto
  });
  this.table.renderRows();

}
updateRowData(row_obj){
  this.data = this.data.filter((value,key)=>{
    if(value.id == row_obj.id){
      value.name = row_obj.name;
    }
    return true;
  });
}
deleteRowData(row_obj){
  this.data = this.data.filter((value,key)=>{
    return value.id != row_obj.id;
  });
   this.GenericService.deleteLocal('producto', row_obj.id).subscribe(
    data1 => {
      this.data1 = data1;
    } );
}


}



