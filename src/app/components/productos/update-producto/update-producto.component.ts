import { Component, OnInit } from '@angular/core';
import { GenericService } from 'src/app/generic.service';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-update-producto',
  templateUrl: './update-producto.component.html',
  styleUrls: ['./update-producto.component.css']
})

export class UpdateProductoComponent implements OnInit {
  formularioProducto: FormGroup;
  data: any;
  data1: any;
  datadesc: any;
  ValidationMessages = {
    'descripcion': [
      { type: 'required', message: 'Descripcion requerida' },

    ],
    'codigo': [
      { type: 'required', message: 'Codigo es requerido' },

    ],

    'precio': [
      { type: 'min', message: 'Precio no puede ser negativo' },
      { type: 'required', message: 'Precio es requerido' },
    ],

    'imagen': [
      { type: 'required', message: 'Imagen es requerida' },

    ],

    'tipo_producto': [
      { type: 'required', message: 'Tipo producto es requerido' },

    ]

    }
  constructor(private GenericService: GenericService,
              private FormBuilder: FormBuilder,
              private rutaActiva: ActivatedRoute,
              private router: Router, private _ac: ActivatedRoute,
              private _snackBar: MatSnackBar)
    {  this.formularioProducto = this.FormBuilder.group({
    id: [this.rutaActiva.snapshot.params.id, Validators.required],
    descripcion: ['', Validators.required],
    codigo: ['', Validators.required],
    precio: ['', Validators.required],
    imagen: ['', Validators.required],
    tipo_producto: ['', Validators.required],
    local: ['', Validators.required]
  });
 }

 ngOnInit() {
  if ( this._ac.snapshot.data.pruebaResolver === null) {
    this.router.navigate(['/home']);
 }
 this.getFilter()

  this.GenericService.getLocalById('producto', this.rutaActiva.snapshot.params.id
  ).subscribe(
  data => {
    this.data = data;
  this.datadesc = data.descripcion;

    this.formularioProducto = this.FormBuilder.group({
      id: [this.rutaActiva.snapshot.params.id, Validators.required],
      descripcion: [data.descripcion, Validators.required],
      codigo:  [data.codigo, Validators.required],
      precio: [data.precio, Validators.compose([Validators.min(0), Validators.required])],
      imagen: [data.imagen, Validators.required],
      tipo_producto: [data.tipo_producto.id, Validators.required],
      local: [data.local.id, Validators.required]

    });
}
);
}
onSubmit() {
  this.GenericService.updateLocal('producto', this.formularioProducto.value).subscribe(
    data => {
      this.data = data;
    }
);  this.ngOnInit();
    this.openSnackBar();
    }
    getFilter() {
      this.GenericService.get('tipoproducto', this._ac.snapshot.data.pruebaResolver.id).subscribe(
          data1 => {
            this.data1 = data1;
    });
}
openSnackBar() {
  this._snackBar.open('Producto ' + this.formularioProducto.value.descripcion + ' actualizado con exito', 'Aceptar', {
    duration: 3000,
  });
}

}

