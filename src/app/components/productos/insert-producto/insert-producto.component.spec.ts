import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InsertProductoComponent } from './insert-producto.component';

describe('InsertProductoComponent', () => {
  let component: InsertProductoComponent;
  let fixture: ComponentFixture<InsertProductoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InsertProductoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InsertProductoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
