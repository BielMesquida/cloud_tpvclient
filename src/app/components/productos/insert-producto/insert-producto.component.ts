import { Component, OnInit } from '@angular/core';
import { GenericService } from 'src/app/generic.service';
import { Validators, FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';

interface Food {
  value: string;
  viewValue: string;
}
@Component({
  selector: 'app-insert-producto',
  templateUrl: './insert-producto.component.html',
  styleUrls: ['./insert-producto.component.css'],

})
export class InsertProductoComponent implements OnInit {
  formularioProducto: FormGroup
  data: any;
  dataCheck: any;
  data1: any = [
  ];
  ValidationMessages = {
    'descripcion': [
      { type: 'required', message: 'Descripcion requerida' },

    ],
    'codigo': [
      { type: 'required', message: 'Codigo es requerido' },

    ],

    'precio': [
      { type: 'min', message: 'Precio no puede ser negativo' },
      { type: 'required', message: 'Precio es requerido' },
    ],

    'imagen': [
      { type: 'required', message: 'Imagen es requerida' },

    ],

    'tipo_producto': [
      { type: 'required', message: 'Tipo producto es requerido' },

    ]

    }
  constructor(private GenericService: GenericService,
               private FormBuilder: FormBuilder,
                private _ac: ActivatedRoute, private router: Router,
                private _snackBar: MatSnackBar ) {
                this.formularioProducto = this.FormBuilder.group({
                  descripcion: ['', Validators.required],
                  codigo:  ['', Validators.required],
                  precio:  ['',  Validators.compose([Validators.min(0), Validators.required])],
                  imagen: ['', Validators.required],
                 tipo_producto: ['', Validators.required],
                 local: [this._ac.snapshot.data.pruebaResolver.id, Validators.required]

                });
            }
  ngOnInit() {
    if ( this._ac.snapshot.data.pruebaResolver === null) {
      this.router.navigate(['/home']);
   }
    this.getFilter();
   }

onSubmit() {

this.GenericService.insertarLocal('producto', this.formularioProducto.value).subscribe(
    data => {
      this.data = data;
    }
);
this.openSnackBar();
    }

getFilter() {
       this.GenericService.get('tipoproducto', this._ac.snapshot.data.pruebaResolver.id).subscribe(
           data1 => {
             this.data1 = data1;
     });
}
openSnackBar() {
  this._snackBar.open('Producto ' + this.formularioProducto.value.descripcion + ' creado con exito', 'Aceptar', {
    duration: 3000,
  });
}


}
