import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { GenericService } from 'src/app/generic.service';

@Component({
  selector: 'app-lineapedido',
  templateUrl: './lineapedido.component.html',
  styleUrls: ['./lineapedido.component.css']
})
export class LineapedidoComponent implements OnInit {
  dataElement: any;
  data1: any;
  constructor(private GenericService: GenericService) { }
  headElements = ['precio', 'cantidad', 'producto','Acciones']
  @Input() idfactura: any;
  ngOnInit() {
    this.GenericService.get('linea_pedido', this.idfactura).subscribe(
      dataElement => {
        this.dataElement = dataElement;
      }
    );
  }
  delete(id) {
    this.GenericService.deleteLocal('linea_pedido', id).subscribe(
      data1 => {
        this.data1 = data1;
        this.ngOnInit();
      }  );

  }
  printLineaPedido(){

  }
}
