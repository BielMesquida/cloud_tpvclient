import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LineapedidoComponent } from './lineapedido.component';

describe('LineapedidoComponent', () => {
  let component: LineapedidoComponent;
  let fixture: ComponentFixture<LineapedidoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LineapedidoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LineapedidoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
