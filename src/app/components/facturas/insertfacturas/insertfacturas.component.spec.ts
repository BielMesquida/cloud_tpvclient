import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InsertfacturasComponent } from './insertfacturas.component';

describe('InsertfacturasComponent', () => {
  let component: InsertfacturasComponent;
  let fixture: ComponentFixture<InsertfacturasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InsertfacturasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InsertfacturasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
