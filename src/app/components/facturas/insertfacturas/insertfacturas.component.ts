import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { GenericService } from 'src/app/generic.service';
import { ActivatedRoute, Router } from '@angular/router';
import { DatePipe } from '@angular/common';


@Component({
  selector: 'app-insertfacturas',
  templateUrl: './insertfacturas.component.html',
  styleUrls: ['./insertfacturas.component.css']
})
export class InsertfacturasComponent implements OnInit {

  formularioFactura: FormGroup;
  data: any;
  data1: any = [

  ];
  fechaactual: any;
  myDate = new Date();
  constructor(private GenericService:GenericService, private FormBuilder: FormBuilder,
              private _ac: ActivatedRoute, private router: Router, private DatePipe: DatePipe) {
            this.fechaactual = this.DatePipe.transform(this.myDate, 'yyyy-MM-dd hh:mm:ss');

            this.formularioFactura = this.FormBuilder.group({
    fecha: [this.fechaactual, Validators.required],
    total: ['', Validators.required],
    mesa: ['', Validators.required],
  });

}

  ngOnInit() {
    if ( this._ac.snapshot.data.pruebaResolver === null) {
      this.router.navigate(['/home']);
   }
    this.getFilter();
  }

  onSubmit() {

    this.GenericService.insertarLocal('ticket', this.formularioFactura.value).subscribe(
        data => {
          this.data = data;
        }
    );
        }

   getFilter() {
    this.GenericService.get('mesa', this._ac.snapshot.data.pruebaResolver.id).subscribe(
            data1 => {
            this.data1 = data1;
        });
   }

    }
