import { Component, OnInit } from '@angular/core';
import { GenericService } from 'src/app/generic.service';
import { LoginService } from 'src/app/services/login.service';
import { ActivatedRoute, Router } from '@angular/router';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { PageEvent, MatDialog } from '@angular/material';
import { DialogBoxComponent } from 'src/app/dialog-box/dialog-box.component';
import { DatePipe } from '@angular/common';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
pdfMake.vfs = pdfFonts.pdfMake.vfs;
export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}


@Component({
  selector: 'app-facturas',
  templateUrl: './facturas.component.html',
  styleUrls: ['./facturas.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],

})


export class FacturasComponent implements OnInit {
  headElements: string[] = ['Fecha', 'Total', 'Mesa', 'Estado', 'Acciones'];

  constructor(private GenericService: GenericService, private LoginService: LoginService,
    private _ac: ActivatedRoute, private router: Router, private pageEvent: PageEvent, public dialog: MatDialog,
    private DatePipe: DatePipe) { }
  data: any;
  dataCheck: any;
  data1: any;
  dataElement: any;
  data12: any;
  expandedElement: PeriodicElement | null;
  pageSizeOptions: number[] = [10, 5, 25, 100];
  public lineaPedido: any = [];
  public imprimirData: any;

  ngOnInit() {
    if (this._ac.snapshot.data.pruebaResolver === null) {
      this.router.navigate(['/home']);
    }
    this.GenericService.getProductos('ticket', this._ac.snapshot.data.pruebaResolver.id, 0, 10)
      .subscribe(
        data => {
          this.data = data.content;
          this.data12 = data.totalElements;
        });
  }

  setPage(e: PageEvent) {
    this.pageEvent.pageIndex = e.pageIndex;
    this.pageEvent.pageSize = e.pageSize;
    this.GenericService.getProductos('ticket', this._ac.snapshot.data.pruebaResolver.id,
      this.pageEvent.pageIndex, this.pageEvent.pageSize).subscribe(
        data => {
          this.data = data.content;
          this.data12 = data.totalElements;
        });
  }
  getFilter() {
    this.GenericService.get('ticket', this._ac.snapshot.data.pruebaResolver.id).subscribe(
      data => {
        this.data = data;

      });

  }

  delete(id) {
    this.GenericService.deleteLocal('ticket', id).subscribe(
      data1 => {
        this.data1 = data1;
        this.ngOnInit();
      });
  }
  openDialog(action, obj) {
    obj.action = action;
    const dialogRef = this.dialog.open(DialogBoxComponent, {
      width: '250px',
      data: obj
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result.event == 'Delete') {
        this.deleteRowData(result.data);
      }
    });
  }
  deleteRowData(row_obj) {
    this.data = this.data.filter((value, key) => {
      return value.id != row_obj.id;
    });
    this.GenericService.deleteLocal('ticket', row_obj.id).subscribe(
      data1 => {
        this.data1 = data1;
      });
  }

  imprimirFactura(factura) {
    this.lineaPedido = [];
    this.lineaPedido.push(['Cantidad', 'Descripcion', 'Precio unitario']);

    var dd;
    this.GenericService.get('linea_pedido', factura.id).subscribe(
      data1 => {
        this.imprimirData = data1;
          for(let lineaPedido of this.imprimirData){
            this.lineaPedido.push([lineaPedido.cantidad, lineaPedido.producto.descripcion, lineaPedido.precio])
          }
        dd = {
          content: [
            this.imprimirData[0].ticket.mesa.local.descripcion,
            this.imprimirData[0].ticket.mesa.local.nif,
            this.imprimirData[0].ticket.mesa.local.telefono,
            this.imprimirData[0].ticket.mesa.local.email,
            { text: 'Fecha emisión:' + this.DatePipe.transform(factura.fecha, 'yyyy-MM-dd hh:mm:ss')  , fontSize: 10, bold: true, margin: [0, 20, 0, 8] },
            {
              style: 'tableExample',
              table: {
                headerRows: 1,
                body: this.lineaPedido
              },
              layout: 'headerLineOnly'
            },

            { text: 'Total: '+factura.total  , fontSize: 15, bold: true, margin: [0, 20, 0, 8] },

          ]


        }
      //  pdfMake.createPdf(dd).open();
        pdfMake.createPdf(dd).download(this.DatePipe.transform(factura.fecha, 'yyyy-MM-dd hh:mm:ss')+'-'+this.imprimirData[0].ticket.mesa.local.descripcion,
        );

      });
    // playground requires you to assign document definition to a variable called dd



  }

}
