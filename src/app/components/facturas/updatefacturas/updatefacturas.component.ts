import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { GenericService } from 'src/app/generic.service';
import { ActivatedRoute, Router } from '@angular/router';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-updatefacturas',
  templateUrl: './updatefacturas.component.html',
  styleUrls: ['./updatefacturas.component.css']
})
export class UpdatefacturasComponent implements OnInit {

  formularioFactura: FormGroup;
  data: any;
  data1: any = [

  ];
  fechaactual: any;
  myDate = new Date();
  ValidationMessages = {
    'fecha': [
      { type: 'required', message: 'Fecha requerida' },

    ],
    'total': [
      { type: 'required', message: 'Total es requerido' },
      { type: 'min', message: 'Total no puede ser negativo' },
    ],

    'estado': [
      { type: 'required', message: 'Estado es requerido' },

    ],

    'mesa': [
      { type: 'required', message: 'Imagen es requerida' },

    ],

    }
  constructor(private GenericService: GenericService,
              private FormBuilder: FormBuilder,
              private rutaActiva: ActivatedRoute,
              private DatePipe: DatePipe,
              private router: Router, private _ac: ActivatedRoute) {
      this.formularioFactura = this.FormBuilder.group({
        id: [this.rutaActiva.snapshot.params.id, Validators.required],
        fecha: ['', Validators.required],
        total: ['', Validators.compose([Validators.required, Validators.min(0)])],
        estado:['', Validators.required],
        mesa: ['', Validators.required],
      });
    }

 ngOnInit() {
  if ( this._ac.snapshot.data.pruebaResolver === null) {
    this.router.navigate(['/home']);
 }
  this.getFilter();
  this.GenericService.getLocalById('ticket', this.rutaActiva.snapshot.params.id
  ).subscribe(
  data => {
    this.data = data;
    this.fechaactual = this.DatePipe.transform(new Date(), 'yyyy-MM-dd hh:mm:ss');
    this.formularioFactura = this.FormBuilder.group({
      id: [this.rutaActiva.snapshot.params.id, Validators.required],
      fecha: [this.fechaactual, Validators.required],
      total: [data.total, Validators.compose([Validators.required, Validators.min(0)])],
      estado:[data.pagado, Validators.required],
      mesa: [data.mesa.id, Validators.required],
    });
  }

);
}
onSubmit() {
  this.GenericService.updateLocal('ticket', this.formularioFactura.value).subscribe(
    data => {
      this.data = data;
    }
);
    }

    getFilter() {
      this.GenericService.get('mesa', this._ac.snapshot.data.pruebaResolver.id).subscribe(
              data1 => {
              this.data1 = data1;
          });
     }

}

