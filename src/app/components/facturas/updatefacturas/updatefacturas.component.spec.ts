import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdatefacturasComponent } from './updatefacturas.component';

describe('UpdatefacturasComponent', () => {
  let component: UpdatefacturasComponent;
  let fixture: ComponentFixture<UpdatefacturasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdatefacturasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdatefacturasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
