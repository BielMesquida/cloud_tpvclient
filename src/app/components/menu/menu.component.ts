import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { LoginService } from '../../services/login.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { pruebaResolver } from '../../resolvers/prueba.resolver';
@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  gif: boolean;
  deleteData: any;
  dataCheck = this._ac.snapshot.data.pruebaResolver;
  constructor(private LoginService: LoginService, private router: Router,
    private titleService: Title, private _ac: ActivatedRoute /*private LoginComponent: LoginComponent*/) {
   this.gif = false;
 }
 public setTitle( newTitle: string) {
   this.titleService.setTitle( newTitle );

 }

 ngOnInit() {

}
 onDelete() {
   this.LoginService.deleteSession().subscribe(
     deleteData => {
       this.deleteData = deleteData;
       this.dataCheck = null;
       if (this.deleteData === true) {
         this.router.navigate(['/home']);
       }
     }
   );
 }

 onCheck() {
   this.LoginService.getCheck().subscribe(
     dataCheck => {
       this.dataCheck = dataCheck;
       }
     );

 }
 botonPrueba() {
   this.router.navigate(['/mesas/']);
 }


}
