import { Component, OnInit, AfterViewInit } from '@angular/core';
import { GenericService } from 'src/app/generic.service';
import { LoginService } from 'src/app/services/login.service';
import { ActivatedRoute, Router } from '@angular/router';
import { PageEvent, MatDialog } from '@angular/material';
import { DialogBoxComponent } from 'src/app/dialog-box/dialog-box.component';
@Component({
  selector: 'app-tipoproducto',
  templateUrl: './tipoproducto.component.html',
  styleUrls: ['./tipoproducto.component.css']
})
export class TipoproductoComponent implements OnInit {

  constructor(private GenericService: GenericService, private LoginService: LoginService,
              private _ac: ActivatedRoute, private router: Router, private pageEvent: PageEvent,
              private dialog: MatDialog ) { }
  data: any;
  dataCheck: any;
  data1: any;
  data12: any;
  headElements = [ 'Descripcion', 'Acciones' ];
  length = 100;
  pageSizeOptions: number [] = [ 10, 5, 25, 100];
  ngOnInit() {
    if ( this._ac.snapshot.data.pruebaResolver === null) {
      this.router.navigate(['/home']);
      return;
   }
   this.GenericService.getProductos('tipoproducto', this._ac.snapshot.data.pruebaResolver.id,  0, 10 )
   .subscribe(
    data => {
      this.data = data.content;
      this.data12 = data.totalElements;
});
  }

  delete(id) {
    this.GenericService.deleteLocal('tipoproducto', id).subscribe(
      data1 => {
        this.data1 = data1;
        this.ngOnInit();
      }  );

  }
  getFilter(){
    this.GenericService.getProductos('tipoproducto', this._ac.snapshot.data.pruebaResolver.id,  0, 10 )
    .subscribe(
     data => {
       this.data = data.content;
       this.data12 = data.totalElements;
});
  }

  setPage(e:PageEvent){
    this.pageEvent.pageIndex = e.pageIndex;
    this.pageEvent.pageSize = e.pageSize;
    this.GenericService.getProductos('tipoproducto', this._ac.snapshot.data.pruebaResolver.id,
    this.pageEvent.pageIndex, this.pageEvent.pageSize ).subscribe(
      data => {
        this.data = data.content;
        this.data12 = data.totalElements;
});
  }

openDialog(action,obj) {
  obj.action = action;
  const dialogRef = this.dialog.open(DialogBoxComponent, {
    width: '250px',
    data:obj
  });
  dialogRef.afterClosed().subscribe(result => {
    if(result.event == 'Delete'){
      this.deleteRowData(result.data);
    }
  });
}

deleteRowData(row_obj){
  this.data = this.data.filter((value,key)=>{
    return value.id != row_obj.id;
  });
   this.GenericService.deleteLocal('tipoproducto', row_obj.id).subscribe(
    data1 => {
      this.data1 = data1;
    } );



}
}
