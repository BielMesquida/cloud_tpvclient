import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdatetipoproductoComponent } from './updatetipoproducto.component';

describe('UpdatetipoproductoComponent', () => {
  let component: UpdatetipoproductoComponent;
  let fixture: ComponentFixture<UpdatetipoproductoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdatetipoproductoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdatetipoproductoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
