import { Component, OnInit } from '@angular/core';
import { GenericService } from 'src/app/generic.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-updatetipoproducto',
  templateUrl: './updatetipoproducto.component.html',
  styleUrls: ['./updatetipoproducto.component.css']
})
export class UpdatetipoproductoComponent implements OnInit {
  formularioTipoProducto: FormGroup;
  data: any;
  datadesc: any;
  constructor(private GenericService: GenericService,
              private FormBuilder: FormBuilder,
              private rutaActiva: ActivatedRoute,
              private _ac:ActivatedRoute,
              private router: Router,
              private _snackBar: MatSnackBar) {
      this.formularioTipoProducto = this.FormBuilder.group({
        id: [this.rutaActiva.snapshot.params.id, Validators.required],
        descripcion: ['', Validators.required],
        local: ['', Validators.required]

      });
  }
  ngOnInit() {
    if ( this._ac.snapshot.data.pruebaResolver === null) {
      this.router.navigate(['/home']);
   }

    this.GenericService.getLocalById('tipoproducto', this.rutaActiva.snapshot.params.id
    ).subscribe(
    data => {
      this.data = data;
      this.datadesc = data.descripcion;
      this.formularioTipoProducto = this.FormBuilder.group({
        id: [this.rutaActiva.snapshot.params.id, Validators.required],
        descripcion: [data.descripcion, Validators.required],
        local: [data.local.id, Validators.required]

      });
  }
  );
  }

  onSubmit() {
    this.GenericService.updateLocal('tipoproducto', this.formularioTipoProducto.value).subscribe(
      data => {
        this.data = data;
     this.openSnackBar();
     this.ngOnInit();
      }

  );
      }
      openSnackBar() {
        this._snackBar.open('Tipo de producto: ' + this.formularioTipoProducto.value.descripcion + ' actualizado con exito', 'Aceptar', {
          duration: 3000,
        });
      }
  }


