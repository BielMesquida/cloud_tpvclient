import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InserttipoproductoComponent } from './inserttipoproducto.component';

describe('InserttipoproductoComponent', () => {
  let component: InserttipoproductoComponent;
  let fixture: ComponentFixture<InserttipoproductoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InserttipoproductoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InserttipoproductoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
