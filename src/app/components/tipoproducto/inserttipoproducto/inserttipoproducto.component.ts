import { Component, OnInit } from '@angular/core';
import { GenericService } from 'src/app/generic.service';
import { Validators, FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-inserttipoproducto',
  templateUrl: './inserttipoproducto.component.html',
  styleUrls: ['./inserttipoproducto.component.css']
})
export class InserttipoproductoComponent implements OnInit {
  formularioTipoProducto: FormGroup
  data: any;
  dataCheck: any;
  data1: any = [

  ];
  constructor(private GenericService: GenericService,
    private FormBuilder: FormBuilder,
     private _ac: ActivatedRoute, private router: Router, private _snackBar: MatSnackBar ) {
     this.formularioTipoProducto = this.FormBuilder.group({
       descripcion: ['', Validators.required],

      local: [this._ac.snapshot.data.pruebaResolver.id, Validators.required]

     });
 }

 ngOnInit() {
  if ( this._ac.snapshot.data.pruebaResolver === null) {
    this.router.navigate(['/home']);
 }
 }

onSubmit() {

this.GenericService.insertarLocal('tipoproducto', this.formularioTipoProducto.value).subscribe(
  data => {
    this.data = data;
  this.openSnackBar();
  }
);
  }


  openSnackBar() {
    this._snackBar.open('Tipo de producto: ' + this.formularioTipoProducto.value.descripcion + ' creado con exito', 'Aceptar', {
      duration: 3000,
    });
  }

}
