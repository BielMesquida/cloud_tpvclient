import { Component, OnInit, Input } from '@angular/core';
import { GenericService } from 'src/app/generic.service';

import { Router, CanActivate, Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, ActivatedRoute } from '@angular/router';
import { LoginService } from 'src/app/services/login.service';
import { Observable, of, EMPTY } from 'rxjs';
import { take, mergeMap } from 'rxjs/operators';
import { DialogBoxComponent } from 'src/app/dialog-box/dialog-box.component';
import { MatDialog } from '@angular/material';
import { pruebaResolver } from 'src/app/resolvers/prueba.resolver';

@Component({
  selector: 'app-mesas',
  templateUrl: './mesaLocal.component.html',
})

export class checkMesaLocalComponent implements OnInit {

  public idMesaLocal: string;
  constructor(private GenericService: GenericService, private LoginService: LoginService,
             private router: Router, private _ac: ActivatedRoute, private dialog: MatDialog, private resolver: pruebaResolver) {
             }

 ngOnInit() {
  if ( this._ac.snapshot.data.pruebaResolver === null) {

  }
 }
  public redireccionarMesa(){
    let idMesa;
    let idLocal;
    idMesa = this.idMesaLocal.substr(0,2);
    idLocal = this.idMesaLocal.substr(2,2);
    this.resolver.idMesaLocal = this.idMesaLocal;
    this.resolver.redirectMesas();
  }



}
