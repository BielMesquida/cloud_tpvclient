import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Lineapedido2Component } from './lineapedido2.component';

describe('Lineapedido2Component', () => {
  let component: Lineapedido2Component;
  let fixture: ComponentFixture<Lineapedido2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Lineapedido2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Lineapedido2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
