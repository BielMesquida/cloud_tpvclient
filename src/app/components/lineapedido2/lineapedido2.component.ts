import { Component, OnInit } from '@angular/core';
import { Router, CanActivate, Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, ActivatedRoute } from '@angular/router';
import { GenericService } from 'src/app/generic.service';

@Component({
  selector: 'app-lineapedido2',
  templateUrl: './lineapedido2.component.html',
  styleUrls: ['./lineapedido2.component.css']
})
export class LineapedidoComponent implements OnInit {

  constructor(private GenericService: GenericService, private router: Router, private _ac: ActivatedRoute,) { }
dataLp:any;
dataLpCantidad: any;
dataLpLength: any;
  ngOnInit() {
    if ( this._ac.snapshot.data.pruebaResolver === null) {
      this.router.navigate(['/home']);
   }
    this.getelepe();
  }
  getelepe(){
    this.GenericService.getLp('linea_pedido', 49 )
    .subscribe(
     dataLp => {
       this.dataLp = dataLp;
       this.dataLpCantidad = dataLp.cantidad;
       this.dataLpLength = dataLp.length;
     //  this.dataLpid = dataLp[0].ticket;
    });
    }
}
