import { Component, OnInit } from '@angular/core';
import { GenericService } from '../../generic.service';
import { Router, CanActivate, ActivatedRoute } from '@angular/router';
import { AppComponent } from 'src/app/app.component';

@Component({
  selector: 'app-local',
  templateUrl: './local.component.html',
  styleUrls: ['./local.component.css'],
})

export class LocalComponent implements OnInit {
  data: any = [] ;
  data1: any;
  hola: any;
  headElements = [ 'ID', 'Descripcion', 'Nif', 'Email', 'Telefono', 'Login' ];
  constructor(private GenericService: GenericService, private router: Router, private AppComponent: AppComponent, private _ac: ActivatedRoute) { }

  ngOnInit() {
    if ( this._ac.snapshot.data.pruebaResolver === null) {
      this.router.navigate(['/home']);
   }else
   if ( this._ac.snapshot.data.pruebaResolver.id === 1  ) {
    this.GenericService.getMesas('local').subscribe(
      data => {
        this.data = data;
      }
    );
  }else{
    this.router.navigate(['/home']);

  }

  }

delete(id) {
  this.GenericService.deleteLocal('local', id).subscribe(
    data1 => {
      this.data1 = data1;
      this.ngOnInit();
    }  );
}
}
