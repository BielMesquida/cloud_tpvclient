import { Component, OnInit } from '@angular/core';
import { GenericService } from 'src/app/generic.service';
import { Validators, FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-insertlocal',
  templateUrl: './insertlocal.component.html',
  styleUrls: ['./insertlocal.component.css']
})
export class InsertlocalComponent implements OnInit {

  formularioLocal: FormGroup

  data: any;


  constructor(private GenericService: GenericService,
               private FormBuilder: FormBuilder, private _ac: ActivatedRoute, private router: Router) {

                this.formularioLocal = this.FormBuilder.group({

                  descripcion: ['', Validators.required],
                  nif:  ['', Validators.required],
                  email:  ['', Validators.required],
                  telefono: ['', Validators.required],
                  login:  ['', Validators.required],
                  password: ['', Validators.required],
                });
            }
  ngOnInit() {
    if ( this._ac.snapshot.data.pruebaResolver === null) {
      this.router.navigate(['/home']);
   }
  }

onSubmit() {

this.GenericService.insertarLocal('local', this.formularioLocal.value).subscribe(
    data => {
      this.data = data;
    }
);
    }

}




