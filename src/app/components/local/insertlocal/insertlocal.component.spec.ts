import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InsertlocalComponent } from './Insertlocal.Component';

describe('InsertlocalComponent', () => {
  let component: InsertlocalComponent;
  let fixture: ComponentFixture<InsertlocalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InsertlocalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InsertlocalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
