import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { GenericService } from 'src/app/generic.service';
import { ActivatedRoute, Params, Router } from '@angular/router';

@Component({
  selector: 'app-updatelocal',
  templateUrl: './updatelocal.component.html',
  styleUrls: ['./updatelocal.component.css']
})
export class UpdatelocalComponent implements OnInit {

  formularioLocal: FormGroup;
  data: any;
  descripcion: any;


  constructor(private GenericService: GenericService,
              private FormBuilder: FormBuilder,
              private rutaActiva: ActivatedRoute,
              private _ac: ActivatedRoute, private router: Router) {

                this.formularioLocal = this.FormBuilder.group({
                  id: [this.rutaActiva.snapshot.params.id, Validators.required],
                  descripcion: ['', Validators.required],
                  nif:  ['', Validators.required],
                  email:  ['', Validators.required],
                  telefono: ['', Validators.required],
                  login:  ['', Validators.required],
                  password: ['', Validators.required],
                });
            }

            ngOnInit() {
              if ( this._ac.snapshot.data.pruebaResolver === null) {
                this.router.navigate(['/home']);
             }

              this.GenericService.getLocalById('local', this.rutaActiva.snapshot.params.id
                ).subscribe(
                data => {
                  this.data = data;
                  this.formularioLocal = this.FormBuilder.group({
                    id: [this.rutaActiva.snapshot.params.id, Validators.required],
                    descripcion: [data.descripcion, Validators.required],
                    nif:  [data.nif, Validators.required],
                    email:  [data.email, Validators.required],
                    telefono: [data.telefono, Validators.required],
                    login:  [data.login, Validators.required],
                    password: ['', Validators.required],
                  });
              }
              );

            }

onSubmit() {

this.GenericService.updateLocal('local', this.formularioLocal.value).subscribe(
    data => {
      this.data = data;
    }
);
    }

}


