import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdatelocalComponent } from './updatelocal.component';

describe('UpdatelocalComponent', () => {
  let component: UpdatelocalComponent;
  let fixture: ComponentFixture<UpdatelocalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdatelocalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdatelocalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
