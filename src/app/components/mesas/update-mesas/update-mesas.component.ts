import { Component, OnInit } from '@angular/core';
import { GenericService } from 'src/app/generic.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { AppComponent } from 'src/app/app.component';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-update-mesas',
  templateUrl: './update-mesas.component.html',
  styleUrls: ['./update-mesas.component.css']
})
export class UpdateMesasComponent implements OnInit {
  formularioMesas: FormGroup;
  data: any;
  dataid: any;
  dataMesa: any;
  datanum: any;
  s: any;
   mesasValidationMessages = {
    'numero': [
      { type: 'required', message: 'Campo requerido' },
      { type: 'max', message: 'El número máximo és 999' },
      { type: 'min', message: 'No se admiten valores negativos'}
    ]
    }
  constructor(private GenericService: GenericService,
    private FormBuilder: FormBuilder,
    private rutaActiva: ActivatedRoute,
    private AppComponent:AppComponent
    ,private _ac:ActivatedRoute, private router: Router,
    private _snackBar: MatSnackBar) {
      this.formularioMesas = this.FormBuilder.group({
        id: [this.rutaActiva.snapshot.params.id, Validators.required],
        numero: [''],
        local_id: ['', Validators.required],

      });
  }
  ngOnInit() {
    if ( this._ac.snapshot.data.pruebaResolver === null) {
      this.router.navigate(['/home']);
   }

    this.GenericService.getLocalById('mesa', this.rutaActiva.snapshot.params.id
    ).subscribe(
    data => {
      this.data = data;
      this.datanum = data.numero;
      this.dataid = data.id;
      this.formularioMesas = this.FormBuilder.group({
        id: [this.rutaActiva.snapshot.params.id, Validators.required],
        numero: [data.numero, Validators.compose([Validators.max(999),Validators.min(0), Validators.required])],
        local_id:  [data.local.id, Validators.required],

      });
  }
  );
  }

  onSubmit() {
    this.s = 0;
    this.GenericService.get('mesa', this._ac.snapshot.data.pruebaResolver.id).subscribe(
    data => {
      this.data = data;
      for (this.dataMesa of data) {
        if (this.dataMesa.numero === this.formularioMesas.value.numero) {
                this.s = 1;
          }
        }
      if (this.s === 0) {
           this.GenericService.updateLocal('mesa', this.formularioMesas.value).subscribe(
            data => {
              this.data = data;
            this.openSnackBar1();
            this.ngOnInit();
            }
        );
        } else {
          this.openSnackBar();
        }
});


    }
    openSnackBar1() {
      this._snackBar.open(' Mesa numero ' + this.formularioMesas.value.numero + ' actualizada con exito', 'Aceptar', {
        duration: 3000,
      });
    }
    openSnackBar() {
      this._snackBar.open('Error: Mesa numero ' + this.datanum + ' ya existe', 'Aceptar', {
        duration: 3000,
      });
    }
  }


