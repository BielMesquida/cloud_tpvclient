import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InsertMesasComponent } from './insert-mesas.component';

describe('InsertMesasComponent', () => {
  let component: InsertMesasComponent;
  let fixture: ComponentFixture<InsertMesasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InsertMesasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InsertMesasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
