import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { GenericService } from 'src/app/generic.service';
import { AppComponent } from 'src/app/app.component';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-insert-mesas',
  templateUrl: './insert-mesas.component.html',
  styleUrls: ['./insert-mesas.component.css']
})
export class InsertMesasComponent implements OnInit {
  formularioMesas: FormGroup;
  data: any;
  dataMesa: any;
  s: any;

  mesasValidationMessages = {
    numero: [
      { type: 'required', message: 'Campo requerido' },
      { type: 'max', message: 'El número máximo és 999' },
      { type: 'min', message: 'No se admiten valores negativos'}
    ]
    };
  constructor(private GenericService: GenericService, AppComponent: AppComponent, private FormBuilder: FormBuilder
    ,         private _ac: ActivatedRoute, private router: Router, private _snackBar: MatSnackBar) {
    this.formularioMesas = this.FormBuilder.group({
    numero: ['', Validators.compose([Validators.max(999), Validators.min(0), Validators.required])],
    local_id:  [this._ac.snapshot.data.pruebaResolver.id, Validators.required],
  });

}
  ngOnInit() {
    if ( this._ac.snapshot.data.pruebaResolver === null) {
      this.router.navigate(['/home']);
   }
  }

  onSubmit() {
      this.s = 0;
      this.GenericService.get('mesa', this._ac.snapshot.data.pruebaResolver.id).subscribe(
      data => {
        this.data = data;
        for (this.dataMesa of data) {
          if (this.dataMesa.numero === this.formularioMesas.value.numero) {
                  this.s = 1;
            }
          }
        if (this.s === 0) {
             this.GenericService.insertarLocal('mesa', this.formularioMesas.value).subscribe(
              data => {
                this.data = data;
               this.openSnackBar1()
              }
          );
          } else {
           this.openSnackBar()
          }
});
      }
      openSnackBar() {
        this._snackBar.open('Error: Mesa número ' + this.formularioMesas.value.numero + ' ya existe', 'Aceptar', {
          duration: 3000,
        });
      }

      openSnackBar1() {
        this._snackBar.open('Mesa número ' + this.formularioMesas.value.numero + ' creada con exito', 'Aceptar', {
          duration: 3000,
        });
      }
    }

