import { Component, OnInit, Input } from '@angular/core';
import { GenericService } from 'src/app/generic.service';

import { Router, CanActivate, Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, ActivatedRoute } from '@angular/router';
import { LoginService } from 'src/app/services/login.service';
import { Observable, of, EMPTY } from 'rxjs';
import { take, mergeMap } from 'rxjs/operators';
import { DialogBoxComponent } from 'src/app/dialog-box/dialog-box.component';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-mesas',
  templateUrl: './mesas.component.html',
  styleUrls: ['./mesas.component.css']
})

export class MesasComponent implements OnInit {
 // @Input() dataCheck: any;

  constructor(private GenericService: GenericService, private LoginService: LoginService,
             private router: Router, private _ac: ActivatedRoute, private dialog: MatDialog) {
              this.rojo = false;


             }
  headElements: string[] = [ 'Fecha', 'Total', 'Mesa' ];
  data: any ;
  dataCheck: any;
  data1: any;
    rojo: boolean;

 ngOnInit() {
  if ( this._ac.snapshot.data.pruebaResolver === null) {
    this.router.navigate(['/home']);
 }
this.getFilter();

}
getFilter() {
   this.GenericService.get('mesa', this._ac.snapshot.data.pruebaResolver.id).subscribe(
       data => {
         this.data = data;
 });
}

delete(id) {
  this.GenericService.deleteLocal('mesa', id).subscribe(
    data1 => {
      this.data1 = data1;
      this.ngOnInit();
    }  );

}

openDialog(action,obj) {
  obj.action = action;
  const dialogRef = this.dialog.open(DialogBoxComponent, {
    width: '250px',
    data:obj
  });
  dialogRef.afterClosed().subscribe(result => {
    if(result.event == 'Delete'){
      this.deleteRowData(result.data);
    }
  });
}

deleteRowData(row_obj){

   this.GenericService.deleteLocal('mesa', row_obj.id).subscribe(
    data1 => {
      this.data1 = data1;
this.getFilter();
    } );


}
}



