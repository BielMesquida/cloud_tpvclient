import { Component, OnInit } from '@angular/core';
import { GenericService } from 'src/app/generic.service';

import { Router, ActivatedRoute } from '@angular/router';
import { LoginService } from 'src/app/services/login.service';

import { DatePipe } from '@angular/common';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { PageEvent, MatDialog } from '@angular/material';
import { DialogBoxComponent } from 'src/app/dialog-box/dialog-box.component';
import { pruebaResolver } from 'src/app/resolvers/prueba.resolver';

@Component({
  selector: 'app-mesa',
  templateUrl: './mesa.component.html',
  styleUrls: ['./mesa.component.css']
})
export class MesaComponent implements OnInit {
  formularioFactura: FormGroup;
  formularioFactura1: FormGroup;
  fechaactual: any;
  myDate = new Date();
  formularioLp: FormGroup;

  constructor(private GenericService: GenericService, private FormBuilder: FormBuilder, private LoginService: LoginService,
    private router: Router, private _ac: ActivatedRoute, private DatePipe: DatePipe, private pageEvent: PageEvent,
    public dialog: MatDialog, public resolver: pruebaResolver) {
  }
  public headElements;

  pageSizeOptions: number[] = [10, 5, 25, 100];
  data: any;
  dataCheck: any;
  data1: any;
  data12: any;
  dataLp: any;
  dataTick: any;
  dataTickid: any;
  dataTickEstado: any = [];
  dataIns: any;
  dataIns1: any;
  dataLpLength: any;
  dataLpCantidad: any;
  dataformfactura1: any = [];
  total: any;
  dataBorrarTick: any;
  datosFuera: any;
  login: boolean;
  idMesaLocal: string;
  idMesa: string;
  idLocal: string;
  public cantidadLp: number;
  ngOnInit() {
    this.headElements = ['Producto', 'Precio', 'Cantidad', 'Ticket'];
    this.data12 = [];
    this.idMesa = this._ac.snapshot.paramMap.get('id')
    this.idLocal = this._ac.snapshot.paramMap.get('idLocal')
    if (this._ac.snapshot.data.pruebaResolver === null) {
      this.login = false;
      //  this.idMesaLocal = this.resolver.idLocal;
    } else {
      this.login = true;
      this.headElements.push('Acciones');
      // this.idMesaLocal = this._ac.snapshot.data.pruebaResolver.id;
    }
    this.GenericService.getProductos('producto', this.idLocal, 0, 10)
      .subscribe(
        data => {
          this.data = data.content;
          this.data12 = data.totalElements;
        });

    this.getelepe();
    this.getTicket();
  }

  getTicket() {
    this.GenericService.get('ticket', this.idLocal).subscribe(
      dataTick => {
        this.dataTick = dataTick;
        this.dataTickid = dataTick.mesa;
        this.dataTickEstado = dataTick.pagado;
      });
  }

  getelepe() {
    this.GenericService.getLp('linea_pedido', this.idMesa)
      .subscribe(
        dataLp => {
          this.dataLp = dataLp;
          this.dataLpCantidad = dataLp.cantidad;
          this.dataLpLength = dataLp.length;
          this.total = dataLp.reduce((
            acc,
            obj,
          ) => acc + (obj.precio * obj.cantidad),
            0);
        });
  }

  insertFacturas(precio, producto_id) {
    this.fechaactual = this.DatePipe.transform(this.myDate, 'yyyy-MM-dd hh:mm:ss');
    this.formularioFactura = this.FormBuilder.group({
      fecha: [this.fechaactual, Validators.required],
      total: ['', Validators.required],
      pagado: ['0', Validators.required],
      mesa: [this._ac.snapshot.params.id, Validators.required],
    });
    this.GenericService.insertarLocal('ticket', this.formularioFactura.value).subscribe(
      dataIns => {
        this.dataIns = dataIns;
        this.formularioLp = this.FormBuilder.group({
          cantidad: [1, Validators.required],
          precio: [precio, Validators.required],
          ticket_id: [this.dataIns.id, Validators.required],
          producto_id: [producto_id, Validators.required],
        });
        this.GenericService.insertarLocal('linea_pedido', this.formularioLp.value).subscribe(
          dataIns1 => {
            this.dataIns1 = dataIns1;
            this.getelepe();
          }
        );
      }
    );
  }

  insertLineaPedido(precio, cantidad, producto_id) {
     if (this.dataLp.length === 0) {
      this.insertFacturas(precio, producto_id);
    } else {
      this.formularioLp = this.FormBuilder.group({
        cantidad: [1, Validators.required],
        precio: [precio, Validators.required],
        ticket_id: [this.dataLp[0].ticket.id, Validators.required],
        producto_id: [producto_id, Validators.required],
      });
      this.GenericService.insertarLocal('linea_pedido', this.formularioLp.value).subscribe(
        dataIns1 => {
          this.dataIns1 = dataIns1;
          this.getelepe();
        }
      );
    }
  }

  pagar(id) {
    this.GenericService.getLocalById('ticket', id
    ).subscribe(
      data => {
        this.data = data;
        this.fechaactual = this.DatePipe.transform(new Date(), 'yyyy-MM-dd hh:mm:ss');
        this.formularioFactura1 = this.FormBuilder.group({
          id: [id, Validators.required],
          fecha: [this.fechaactual, Validators.required],
          total: [this.total, Validators.required],
          estado: [true, Validators.required],
          mesa: [data.mesa.id, Validators.required],
        });
        this.GenericService.updateLocal('ticket', this.formularioFactura1.value).subscribe(
          dataformfactura1 => {
            this.dataformfactura1 = dataformfactura1;
            // this.getelepe();
            this.ngOnInit();
          }
        );
      }
    );
  }

  delete(id) {
    if (this.dataLpLength > 1) {
      this.GenericService.deleteLocal('linea_pedido', id).subscribe(
        data1 => {
          this.data1 = data1;
          this.getelepe();

        });
    } else {
      this.GenericService.deleteLocal('linea_pedido', id).subscribe(
        data1 => {
          this.data1 = data1;
          this.GenericService.deleteLocal('ticket', this.dataLp[0].ticket.id).subscribe(
            dataBorrarTick => {
              this.dataBorrarTick = dataBorrarTick;
              this.getelepe();
            }
          );
        }
      );
    }
  }

  setPage(e: PageEvent) {
    this.pageEvent.pageIndex = e.pageIndex;
    this.pageEvent.pageSize = e.pageSize;
    this.GenericService.getProductos('producto', this._ac.snapshot.data.pruebaResolver.id,
      this.pageEvent.pageIndex, this.pageEvent.pageSize).subscribe(
        data => {
          this.data = data.content;
          this.data12 = data.totalElements;
        });
  }


  openDialog(action, obj) {
    obj.action = action;
    const dialogRef = this.dialog.open(DialogBoxComponent, {
      width: '250px',
      data: obj
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result.event == 'Pagar') {
        this.updateRowData(result.data);
      } else if (result.event == 'Delete') {
        this.deleteRowData(result.data);
      }
    });
  }
  updateRowData(row_obj) {
    this.GenericService.getLocalById('ticket', row_obj.id
    ).subscribe(
      data => {
        this.data = data;
        this.fechaactual = this.DatePipe.transform(data.fecha, 'yyyy-MM-dd hh:mm:ss');
        this.formularioFactura1 = this.FormBuilder.group({
          id: [row_obj.id, Validators.required],
          fecha: [this.fechaactual, Validators.required],
          total: [this.total, Validators.required],
          estado: [true, Validators.required],
          mesa: [data.mesa.id, Validators.required],
        });
        this.GenericService.updateLocal('ticket', this.formularioFactura1.value).subscribe(
          dataformfactura1 => {
            this.dataformfactura1 = dataformfactura1;
            this.ngOnInit();
          }
        );
      }
    );
  }
  deleteRowData(row_obj) {
    this.data = this.data.filter((value, key) => {
      return value.id != row_obj.id;
    });
    this.GenericService.deleteLocal('producto', row_obj.id).subscribe(
      data1 => {
        this.data1 = data1;
      });
  }
}



