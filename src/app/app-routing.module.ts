import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LocalComponent } from './components/local/local.component';
import { LoginComponent } from './components/login/login.component';
import { InserthomeComponent } from './home/inserthome/inserthome.component';
import { UpdatehomeComponent } from './home/updatehome/updatehome.component';
import { MesasComponent } from './components/mesas/mesas.component';
import { MesaComponent } from './components/mesas/mesa/mesa.component';
import { InsertMesasComponent } from './components/mesas/insert-mesas/insert-mesas.component';
import { UpdateMesasComponent } from './components/mesas/update-mesas/update-mesas.component';
import { ProductosComponent } from './components/productos/productos.component';
import { InsertProductoComponent } from './components/productos/insert-producto/insert-producto.component';
import { UpdateProductoComponent } from './components/productos/update-producto/update-producto.component';
import { FacturasComponent } from './components/facturas/facturas.component';
import { HomeComponent } from './components/home/home.component';
import { GenericService } from './generic.service';
import { LoginService } from './services/login.service';
import { pruebaResolver } from './resolvers/prueba.resolver';
import { MenuComponent } from './components/menu/menu.component';
import { InsertlocalComponent } from './components/local/insertlocal/insertlocal.component';
import { UpdatelocalComponent } from './components/local/updatelocal/updatelocal.component';
import { CanactivateGuard } from './canactivate.guard';
import { TipoproductoComponent } from './components/tipoproducto/tipoproducto.component';
import { InserttipoproductoComponent } from './components/tipoproducto/inserttipoproducto/inserttipoproducto.component';
import { UpdatetipoproductoComponent } from './components/tipoproducto/updatetipoproducto/updatetipoproducto.component';
import { InsertfacturasComponent } from './components/facturas/insertfacturas/insertfacturas.component';
import { UpdatefacturasComponent } from './components/facturas/updatefacturas/updatefacturas.component';
import { checkMesaLocalComponent } from './components/mesaLocal/mesaLocal.component';

const routes: Routes = [
  { path: '', component: checkMesaLocalComponent, resolve: { pruebaResolver }},
  { path: 'local', component: LocalComponent, resolve: { pruebaResolver}},// canActivate: [LoginService]},
  { path: 'login', component: LoginComponent,  resolve: { pruebaResolver}},
  { path: 'insertLocal', component: InsertlocalComponent, resolve: { pruebaResolver}},
  { path: 'updateLocal/:id', component: UpdatelocalComponent, resolve: { pruebaResolver}},
  { path: 'mesas', component: MesasComponent, resolve: { pruebaResolver}},
  { path: 'mesa/:id/:idLocal', component: MesaComponent,  resolve: { pruebaResolver}},
  { path: 'insertMesas', component: InsertMesasComponent,  resolve: { pruebaResolver }},
  { path: 'updateMesas/:id', component: UpdateMesasComponent, resolve: { pruebaResolver}},
  { path: 'productos', component: ProductosComponent, resolve: { pruebaResolver }},
  { path: 'insertProducto', component: InsertProductoComponent, resolve: { pruebaResolver }},
  { path: 'updateProducto/:id', component: UpdateProductoComponent, resolve: { pruebaResolver}},
  { path: 'facturas', component: FacturasComponent, resolve: { pruebaResolver}},
  { path: 'home', component: HomeComponent, resolve: { pruebaResolver}},
  { path: 'menu', component: MenuComponent, resolve: { pruebaResolver }},
  { path: 'tipoproducto', component: TipoproductoComponent, resolve: { pruebaResolver }},
  { path: 'insertTipoProducto', component: InserttipoproductoComponent, resolve: { pruebaResolver }},
  { path: 'updateTipoProducto/:id', component: UpdatetipoproductoComponent, resolve: { pruebaResolver }},
  { path: 'insertTicket', component: InsertfacturasComponent, resolve: { pruebaResolver }},
  { path: 'updateTicket/:id', component: UpdatefacturasComponent, resolve: { pruebaResolver }},

  { path: '', redirectTo: '/home', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
