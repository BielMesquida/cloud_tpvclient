import { Resolve, ActivatedRoute, Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { LoginService } from '../services/login.service';
import { AppComponent } from '../app.component'
@Injectable({
    providedIn: 'root'

})
export class pruebaResolver implements Resolve<Observable<any>>{
  public idMesaLocal;
  public idMesa;
  public idLocal;
  constructor(private LoginService: LoginService, private activatedRoute: ActivatedRoute, private router: Router){}
 resolve() {
  return this.LoginService.getCheck();
 }

 redirectMesas() {
    this.idMesa = this.idMesaLocal.substr(0,2);
    this.idLocal = this.idMesaLocal.substr(2,2);
    this.router.navigate(['/mesa/',this.idMesa,this.idLocal]);

 }
}
