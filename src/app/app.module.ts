import { BrowserModule, Title } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LocalComponent } from './components/local/local.component';
import { HttpClientModule, HttpClient, HttpClientXsrfModule } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { GenericService } from './generic.service';
import { LoginComponent } from './components/login/login.component';
import { InserthomeComponent } from './home/inserthome/inserthome.component';
import { UpdatehomeComponent } from './home/updatehome/updatehome.component';
import { MesasComponent } from './components/mesas/mesas.component';
import { MesaComponent } from './components/mesas/mesa/mesa.component';
import { InsertMesasComponent } from './components/mesas/insert-mesas/insert-mesas.component';
import { UpdateMesasComponent } from './components/mesas/update-mesas/update-mesas.component';
import { ProductosComponent } from './components/productos/productos.component';
import { InsertProductoComponent } from './components/productos/insert-producto/insert-producto.component';
import { UpdateProductoComponent } from './components/productos/update-producto/update-producto.component';
import { FacturasComponent } from './components/facturas/facturas.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatInputModule } from '@angular/material/input';
import { MatRadioModule } from '@angular/material/radio';
import { MatSliderModule } from '@angular/material/slider';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatNativeDateModule, MatDialogModule, MatButton, MatButtonModule, MatCardModule, MatSnackBar, MatSnackBarModule } from '@angular/material';
import { HomeComponent } from './components/home/home.component';
import { MenuComponent } from './components/menu/menu.component';
import { InsertlocalComponent } from './components/local/insertlocal/insertlocal.component';
import { UpdatelocalComponent } from './components/local/updatelocal/updatelocal.component';
import { pruebaResolver } from './resolvers/prueba.resolver';
import { TipoproductoComponent } from './components/tipoproducto/tipoproducto.component';
import { InserttipoproductoComponent } from './components/tipoproducto/inserttipoproducto/inserttipoproducto.component';
import { UpdatetipoproductoComponent } from './components/tipoproducto/updatetipoproducto/updatetipoproducto.component';
import { UpdatefacturasComponent } from './components/facturas/updatefacturas/updatefacturas.component';
import { InsertfacturasComponent } from './components/facturas/insertfacturas/insertfacturas.component';
import { DatePipe } from '@angular/common';
import {MatTableModule} from '@angular/material/table';
import { MatPaginatorModule, MatPaginatorIntl } from '@angular/material/paginator';
import { LineapedidoComponent } from './components/facturas/lineapedido/lineapedido.component';
import { PageEvent } from '@angular/material';
import { PaginacionComponent } from './components/paginacion/paginacion.component';
import {CustomMatPaginatorIntl } from './paginator-es';
import { DialogBoxComponent } from './dialog-box/dialog-box.component';
import { checkMesaLocalComponent } from './components/mesaLocal/mesaLocal.component';

@NgModule({
  declarations: [
    AppComponent,
    LocalComponent,
    LoginComponent,
    MesasComponent,
    MesaComponent,
    InsertMesasComponent,
    UpdateMesasComponent,
    ProductosComponent,
    InsertProductoComponent,
    UpdateProductoComponent,
    FacturasComponent,
    HomeComponent,
    MenuComponent,
    InsertlocalComponent,
    UpdatelocalComponent,
    TipoproductoComponent,
    InserttipoproductoComponent,
    UpdatetipoproductoComponent,
    UpdatefacturasComponent,
    InsertfacturasComponent,
    LineapedidoComponent,
    PaginacionComponent,
    DialogBoxComponent,
    checkMesaLocalComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatFormFieldModule,
    MatAutocompleteModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatInputModule,
    MatRadioModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatNativeDateModule,
    MatTableModule,
    MatPaginatorModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    FormsModule,
    MatCardModule,
    MatSnackBarModule,
  ],
  entryComponents: [
    DialogBoxComponent
  ],
  providers: [ Title, MatSnackBar,pruebaResolver, DatePipe, PageEvent, PaginacionComponent,
  {provide: MatPaginatorIntl,
  useClass: CustomMatPaginatorIntl }],
  bootstrap: [AppComponent]
})
export class AppModule { }
