import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap, take, publishReplay, refCount, retry, catchError } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';
import { AppComponent } from '../app.component';
import { pruebaResolver } from '../resolvers/prueba.resolver';



@Injectable({
  providedIn: 'root'
})
export class LoginService {
  [x: string]: any;
    constructor(private http: HttpClient, private router: Router, private _ac: ActivatedRoute) {}
    url = 'http://localhost:8082/session/';

  getLogin(loginData: any): Observable<any> {
    return this.http.post(this.url, loginData, {
      withCredentials: true
    }
      ).pipe(retry(1),catchError(this.handleError));;
  }

    deleteSession(): Observable<any> {
      return this.http.delete(this.url, {
        withCredentials: true
      });
    }

 public getCheck(): Observable<any> {
    return this.http.get(this.url, {
      withCredentials: true
    })
   /* .pipe(
      publishReplay(1),
      refCount(),
      take(1)
    );*/
  }
  getMesas(): Observable<any> {
    return this.http.get('http://localhost:8082/local/getall');
  }

  /*canActivate() {
    if ( this._ac.snapshot.data.pruebaResolver !== undefined) {
      this.router.navigate(['/home']);
      return false;
    }
      return true;
  }*/

}

